angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
    $scope.doLogout = function () 
    {
      console.log("as");
            window.localStorage.removeItem("type");
    }

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $location) {
})

.controller('LoginCtrl', function($scope, $state, $location, $ionicLoading, $ionicPopup, $http) {

    $scope.loginData = {};

    $scope.doLogin = function() {
    
    $ionicLoading.show( {
    template: 'Signing in...',
    noBackdrop : true,
        });


    var username = $scope.loginData.usrName;
    var password = $scope.loginData.usrPass;

    $http.get("https://syafiqevo.000webhostapp.com/seaFood/login.php?&username="+username+"&password="+password).
    success(function(data) {

    localStorage.setItem("profileData",JSON.stringify(data));

    var loginData = data;

      if (loginData[0] != undefined)
      {
            window.localStorage.removeItem("type");
              console.log("idd", loginData[0].id);
              $scope.$root.stat1 = false;
              $scope.$root.stat2 = true;

              window.localStorage.setItem("uid",loginData[0].id);
              $state.go('app.home');
      }
        
      else
      {
           $ionicPopup.alert({
           title: 'Error',
           template: 'Incorrect username or password'
         });
      }  

    $ionicLoading.hide();
    })

    .error(function(data) {
          $ionicLoading.hide();
           $ionicPopup.alert({
           title: 'Error',
           template: 'Network failed'
         });
    });

    }


    $scope.doRegister = function () {
    $location.path('/registerTab/register');
  }

  $scope.doAbout = function()
  {
    $ionicPopup.alert({
     title: 'About Us',
     template: 'We Are Student Of Politeknik Sultan Idris Shah In The Tourism Management Course Are Required To Innovate Tourism Application . So, Will Introduce Our Apps Which Specific More About Restaurant And It S Raw Seafood Ingredient In Sabak Bernam District. The Objective Of This Apps Is To Promote Sabak Bernam To Be More Well Known To The Tourist Inbound And Outbound.',
     cssClass: 'about'
   });
  }

  $scope.doHelp = function()
  {
    $ionicPopup.alert({
     title: 'Help',
     template: 'For guest, touch the apps icon to enter, for registered user please login. Upon succesfully login, you will be directed to the main page and the drawer menus could be accessed by swiping form left to right ',
     cssClass: 'about'
   });
  }

  $scope.doGuest = function()
    {
              $scope.$root.stat1 = true;
              $scope.$root.stat2 = false;
      $state.go('app.home');
    }


})

.controller('UploadCtrl', function ($scope, $http, $ionicPopup) {

$scope.uploadData = {};

$scope.doRegister = function()
{
  var uid = localStorage.getItem("uid");
  var name = $scope.uploadData.name;
  var url1 = $scope.uploadData.url;
  var category = $scope.uploadData.category;
  var details = $scope.uploadData.details; https:

    $http.get("https://syafiqevo.000webhostapp.com/seaFood/register.php?&uid="+uid+"&name="+name+"&url="+url1+"&category="+category+"&details="+details).
    success(function(data) {
      
      $scope.data = data;
           $ionicPopup.alert({
           title: 'Success',
           template: 'Item Registered'
         });
      console.log($scope.data);
    });

}

})

.controller('RestaurantCtrl', function($scope, $state, $http) {

    var category = "restaurant"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);
    });

       $scope.items = [{
      id   : '991',
      name: 'Restoran Lembah Bernam',
      details1  : 'Restaurant Lembah Bernam is one of the famous attractions Sabak Bernam district that served exotic meal such as porcupine and deer meat. They also serve freshwater prawn according to its which its have their own grades.',
      details2 : 'Address: Jalan Main Canal, Tali Air 9, Sekinchan, Pasir Panjang, Selangor, Malaysia, Tel: +60 17-285 8603, Hours: 11:00am to 5:00pm (Friday closed)',
      menu1 : 'Rendang porcupine',
      menu2 : 'chillied deer meat',
  },{
   id   : '992',
      name: 'Mee Udang King Original',
      details1  : 'Mee Udang King Original opened in August 1988 and its where Mak Cik at the stall have been cooking this delicious dishes for the more than 26 years. It’s one of the famous Mee Udang in Sabak Bernam District besides Mak Cik Zaiton Mee Udang at Bagan Nakhoda Omar Beach.',
      details2 : 'Address: batu 23 sungai nibong, 45400 Kampong Sekinchang, Malaysia, 012-292 0890, Hour: 10.00 am – 11.00pm',
      menu1 : 'Mee Udang Special',
      menu2 : 'Mee Udang Biasa',
  },
  {
   id   : '993',
      name: 'Laksa Pantai Zaiton',
      details1  : 'LAKSA PANTAI ZAITON RESTAURANT is located at BAGAN NAKHODA OMAR beach which they served best mee udang known at SABAK BERNAM DISTRICT. It has become a main attraction for tourist to come to have a taste the cuisine at the SABAK BERNAM DISTRICT. Its most famous dishes is MEE UDANG with three big fresh prawn.',
      details2 : 'Pantai Bno, Bagan Nakhoda Omar, Malaysia,  016-6970517/013-2718517',
      menu1 : '',
      menu2 : '',
  },{
   id   : '994',
      name: 'Restoran Dorani Bayu Resort',
      details1  : 'Dorani Bayu Resort & Service Sdn. Bhd',
      details2 : 'D Muara Dorani Marine Parks, Jalan Lama Kuala Selangor, 45300, Sungai Besar, Selangor, 45300, Malaysia',
      menu1 : '',
      menu2 : '',    
}];

  localStorage.setItem("resData",JSON.stringify($scope.items ));

$scope.goDetails = function(id)
{
  window.localStorage.setItem("resId", id);
  $state.go('app.restaurantDetails');
}

})

.controller('RestaurantDetailsCtrl', function($scope, $location, $http) {


   $scope.items =  JSON.parse(localStorage.getItem('resData'));


  var id = localStorage.getItem("resId");

if (id == 991)
{
  {
    $scope.stat1 = false;
  $scope.stat2 = true;

    var resDetails = $scope.items;

            angular.forEach(resDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentRes = value;
                }
            }, resDetails);  

  $scope.data = $scope.currentRes;

  $scope.img1 = "img/r1_2.jpg";
  $scope.img2 = "img/r1_3.JPG";

  $scope.img3 = "img/r1_5.jpg";
  $scope.img4 = "img/r1_6.jpg";

  }
}
else if (id == 992)
{
    $scope.stat1 = false;
  $scope.stat2 = true;

    var resDetails = $scope.items;

            angular.forEach(resDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentRes = value;
                }
            }, resDetails);  

  $scope.data = $scope.currentRes;

  $scope.img1 = "img/r2_2.jpg";
  $scope.img2 = "img/r2_3.jpg";

  $scope.img3 = "img/r2_4.jpg";
  $scope.img4 = "img/r2_5.jpg";
}

else if (id == 993)
{
     $scope.stat1 = false;
  $scope.stat2 = true;

    var resDetails = $scope.items;

            angular.forEach(resDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentRes = value;
                }
            }, resDetails);  

  $scope.data = $scope.currentRes;

  $scope.img1 = "img/r3_2.jpg";
  $scope.img2 = "img/r3_3.jpg"; 
}

else if (id == 994)
{
     $scope.stat1 = false;
  $scope.stat2 = true;

    var resDetails = $scope.items;

            angular.forEach(resDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentRes = value;
                }
            }, resDetails);  

  $scope.data = $scope.currentRes;

  $scope.img1 = "img/home1.jpg";
  $scope.img2 = "img/r4_1.png"; 
}

else
{
    $scope.stat1 = true;
  $scope.stat2 = false;

    var category = "restaurant"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);

    });
}

    $scope.initialize = function() {
  //var id = localStorage.getItem("resId");

      //sdsdsdsd
    //var myLatlng = new google.maps.LatLng(3.767531, 100.982445);

      if (id == 991)
      {
        var myLatlng = new google.maps.LatLng(3.573316, 101.116779);
        console.log("hiimasuk 1");
      }
      else if (id == 992)
      {
         var myLatlng = new google.maps.LatLng(3.593740, 101.057195);       
                 console.log("hiimasuk 22");
      }
      else if (id == 993)
      {
         var myLatlng = new google.maps.LatLng(3.766933, 100.872787);     
                 console.log("hiimasuk 33");  
      }
      else if (id == 994)
      {
         var myLatlng = new google.maps.LatLng(3.640199, 101.016253);     
                 console.log("hiimasuk 44");  
      }
      else
      {

    //   $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/houseUserID.php?id="+id).
    // success(function(data) {
    //   console.log("kor ", data);

    //   // $scope.lat = parseFloat(data[0].lat) ;
    //   // $scope.lang = parseFloat(data[0].lang) ;

    //   window.localStorage.setItem("lat", parseFloat(data[0].lat));
    //   window.localStorage.setItem("lang", parseFloat(data[0].lang));
     
    //   //var myLatlng = new google.maps.LatLng(lat , lang);       

    //   //$scope.hDetails = data;
    // });
    var lat = localStorage.getItem("lat");
    var lang = localStorage.getItem("lang");


      var myLatlng = new google.maps.LatLng(lat , lang);       

      }


    //sdsdsd
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map3"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }  
 


})


.controller('MarketCtrl', function($scope, $state, $http) {

      var category = "market"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);
    });

  $scope.goDetails = function(id)
  {
    window.localStorage.setItem("marketId", id);
    $state.go('app.marketDetails');
  }
})

.controller('MarketDetailsCtrl', function($scope, $http) {
  var id = localStorage.getItem("marketId");

  if (id == 881)
  {
  $scope.stat1 = false;
  $scope.stat2 = true;

  $scope.name = "Pasar Awam Sungai Besar";
  $scope.img1 = "img/m1_1.jpg";
  $scope.img2 = "img/m1_2.jpg";
  $scope.details1 = "Pasar Awam Sungai Besar Is One Of The Largest Market In Sabak Beram District Which Local People And Also Tourist Buy Raw Seafood Ingridient And Also Others Groceries.";
  $scope.details2 = "41, Jalan Tp-1, Taman Perdana, 45300 Sungai Besar, Selangor, Malaysia";
  }

  else if (id == 882)
  {
  $scope.stat1 = false;
  $scope.stat2 = true;

  $scope.name = "Pasar Awam Pekan Sabak";
  $scope.img1 = "img/m2_1.png";
  $scope.img2 = "img/m2_2.png";
  $scope.details1 = "Pasar Awam Pekan Sabak Is Another Of Market In Sabak Beram District Which Local People And Also Tourist Buy Raw Seafood Ingridient And Also Others Groceries.";
  $scope.details2 = "Jalan Tan Chwee Teah, Taman Kenangan, 45200 Sabak, Selangor, Malaysia";
  }

  else if (id = 883)
  {
  $scope.stat1 = false;
  $scope.stat2 = true;

  $scope.name = "Pasar Awam Sekinchan";
  $scope.img1 = "img/m3_1.png";
  $scope.img2 = "img/m3_2.png";
  $scope.details1 = "Pasar Awam Pekan Sekinchan Is Another Of Market In Sabak Beram District Which Local People And Also Tourist Buy Raw Seafood Ingridient And Also Others Groceries.";
  $scope.details2 = "Jalan Gereja, 45400 Sekinchan, Selangor, Malaysia";
  }

  else
  {
  $scope.stat1 = true;
  $scope.stat2 = false;

    var category = "market"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);

    });
  }

      $scope.initialize = function() {
  //var id = localStorage.getItem("resId");

      //sdsdsdsd
    //var myLatlng = new google.maps.LatLng(3.767531, 100.982445);

      if (id == 881)
      {
        var myLatlng = new google.maps.LatLng(3.664269, 100.987678);
      }

      else if (id = 882)
      {
        var myLatlng = new google.maps.LatLng(3.768699, 100.982704);
      }

      else if (id = 883)
      {
        var myLatlng = new google.maps.LatLng(3.506563, 101.106012);  
      }
      else
      {

    //   $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/houseUserID.php?id="+id).
    // success(function(data) {
    //   console.log("kor ", data);

    //   // $scope.lat = parseFloat(data[0].lat) ;
    //   // $scope.lang = parseFloat(data[0].lang) ;

    //   window.localStorage.setItem("lat", parseFloat(data[0].lat));
    //   window.localStorage.setItem("lang", parseFloat(data[0].lang));
     
    //   //var myLatlng = new google.maps.LatLng(lat , lang);       

    //   //$scope.hDetails = data;
    // });
    var lat = localStorage.getItem("lat");
    var lang = localStorage.getItem("lang");


      var myLatlng = new google.maps.LatLng(lat , lang);       

      }


    //sdsdsd
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map3"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }  
 

})


.controller('AttractionCtrl', function($scope, $state, $http) {

      var category = "attraction"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);
    });

$scope.goDetails = function(id)
{
  window.localStorage.setItem("attractionId", id);
  $state.go('app.attractionDetails');
}

})

.controller('AttractionDetailsCtrl', function($scope, $location, $http) {

 var id = localStorage.getItem("attractionId");

  if (id == 771)
  {
  $scope.stat1 = false;
  $scope.stat2 = true;

  $scope.name = "Kolam Pancing Sg Apong,Sabak Bernam";
  $scope.img1 = "img/a1_1.jpg";
  $scope.img2 = "img/a1_2.jpg";
  $scope.details1 = "Kolam Pancing Sg Apong is an attraction and heaven for fishing person who loves fishing. The price here is reasonable and also this pond have many types of fish such as barramundi (siakap) and jenahak.";
  $scope.details2 = "Kolam Pancing Sg Apong,sabak Bernam, Bagan Nakhoda Omar, Malaysia";
  }

  else
  {
  $scope.stat1 = true;
  $scope.stat2 = false;

    var category = "attraction"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);

    });
  }

      $scope.initialize = function() {
  //var id = localStorage.getItem("resId");

      //sdsdsdsd
    //var myLatlng = new google.maps.LatLng(3.767531, 100.982445);

      if (id == 771)
      {
        var myLatlng = new google.maps.LatLng(3.784345, 100.824982);
      }
      else
      {

    //   $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/houseUserID.php?id="+id).
    // success(function(data) {
    //   console.log("kor ", data);

    //   // $scope.lat = parseFloat(data[0].lat) ;
    //   // $scope.lang = parseFloat(data[0].lang) ;

    //   window.localStorage.setItem("lat", parseFloat(data[0].lat));
    //   window.localStorage.setItem("lang", parseFloat(data[0].lang));
     
    //   //var myLatlng = new google.maps.LatLng(lat , lang);       

    //   //$scope.hDetails = data;
    // });
    // var lat = localStorage.getItem("lat");
    // var lang = localStorage.getItem("lang");


    //   var myLatlng = new google.maps.LatLng(lat , lang);       

      }


    //sdsdsd
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map3"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }  
 

})



.controller('MenuListCtrl', function($scope, $http) {

          var category = "menu"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);
    });
})

.controller('GalleryCtrl', function($scope, $http) {

    $http.get("https://syafiqevo.000webhostapp.com/seaFood/gallery.php").
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);
    });
})

.controller('TipsCtrl', function($scope, $state, $http) {
            var category = "tips"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);
    });

  $scope.goDetails = function(id)
  {
    window.localStorage.setItem("tipId", id);
    $state.go('app.tipsDetails');
  }
})

.controller('TipsDetailsCtrl', function($scope, $http, $ionicModal) {
  var id = localStorage.getItem("tipId");

$scope.vid = true;
  if(id == 661)
  {
    $scope.vid = false;
    $scope.stat1 = false;
    $scope.vidUrl = "https://www.youtube.com/embed/tcyg5Njzckw";
    $scope.stat2 = true;

    $scope.img1 = "img/t1_1.jpg";
    $scope.img2 = "img/t1_2.jpg";

    $scope.name = "How To Choose A Fresh Fish";
    $scope.details1 = " • The eyes should be bright, clear, and convex, never cloudy or sunken.";
    $scope.details2 = " • If the fish has any noticeable odor, it should be briny and of the sea, like seaweed. Anything noticeably pungent, fishy, or similar to the scent of a beach at low tide should be avoided, as this indicates decay, and the off-putting aroma will only be intensified by cooking.";
    $scope.details3 = " • One of the best indicators of freshness are the gills: they should be bright red.";
    $scope.details4 = " • Skin ought to be taut, clean, and glistening, almost as if the fish were still alive. Skin color is not necessarily indicative of the fish's state of decay, as with many varieties the color will fade almost immediately after death.";
    $scope.details5 = " • The belly should be taut, not swollen or sunken. A distended or shriveled belly indicates that the digestive enzymes from the fish's gut have broken down and essentially digested some of the flesh.";
  }

  else if (id == 662)
  {
      $scope.stat1 = false;
  $scope.stat2 = true;

    $scope.img1 = "img/t2_1.jpg";
    $scope.img2 = "img/t2_2.jpg";
    $scope.name = "Mentarang";
    $scope.details1 = " • Mentarang snail is a type of shelled animals ( Shellfish ) edible. Mentarang snails have thin skin and usually prefer water with low temperatures. Its scientific name is Pholas orientalis";
    $scope.details2 = " • Snail Mentarang can only be found in a muddy beach just like in Perlis or Pantai Sabak, Selangor and usually it collected during low tide.";
    $scope.details3 = " • It is also considered as a sex stimulant properties";
  }
  else if (id == 663)
  {
    $scope.stat1 = false;
    $scope.stat2 = true; https:

    $scope.vid = false;
    $scope.vidUrl = "https://www.youtube.com/embed/1v__umd7ZDk";

    $scope.img1 = "img/t3_1.jpg";
    $scope.img2 = "";
    $scope.name = "Five-Spice Crisp-Fried Squid";
    $scope.details1 = " • In most Chinese restaurants, so-called “Salt and Pepper-Style” shrimp or squid usually contain other spices too. A good dose of 5-spice mixture makes these fried squid especially tasty, and dusting them with cornstarch before frying keeps them delicately crisp. Maintain the oil temperature at 375 degrees, and don’t try to fry too many pieces at once. Featured in: Squid: Make It Tender, Make It Quick.";
    $scope.details2 = " • Ingredients : 1 ½ pounds cleaned squid, bodies cut crosswise into 1-inch pieces, tentacles halved, Vegetable oil, for frying, 1 cup cornstarch, ⅛ teaspoon cayenne, 1 teaspoon kosher salt, ½ teaspoon black pepper, ½ teaspoon five-spice powder, 1 or 2 serrano chiles, split lengthwise and sliced thinly crosswise, Cilantro sprigs, for garnish, Lime wedges, for serving";
    $scope.details3 = " • Preparation : ";
    $scope.details4 = "1. Rinse squid well in cold water, then drain well and pat dry with kitchen towels.";
    $scope.details5 = "2. Heat 2 inches of vegetable oil in a wok or cast-iron pan. Adjust heat to maintain 375 degrees.";
    $scope.details6 = "3. In a medium bowl, stir together cornstarch, cayenne, salt, black pepper and five-spice powder. Set bowl near stove.";
    $scope.details7 = "4. Dip squid pieces into cornstarch mixture, one by one, and immediately slip carefully into hot oil. Let fry, making sure not to crowd the pan. (Use 2 pans or work in batches if necessary.) Turn pieces with tongs to ensure even cooking. When squid is lightly browned, after about 2 minutes, remove pieces and blot on paper towels.";
    $scope.details8 = "5. Transfer squid to a serving bowl and sprinkle with chiles. Garnish with cilantro and serve with lime wedges.";
  }

  else if (id == 664)
  {
    $scope.stat1 = false;
    $scope.stat2 = true;

    $scope.img1 = "img/t4_1.jpeg";
    $scope.img2 = "";
    $scope.name = "Chilli crab";
    $scope.details1 = " • Ingredients : 2 teaspoons peanut oil, 4 green (uncooked) blue swimmer crabs, 2 garlic cloves, crushed 2 fresh long red chillies (or 1-2 fresh red birdseye chillies for extra heat), finely chopped, 1 tablespoon finely grated fresh ginger, 60ml (1/4 cup) tomato sauce, 2 tablespoons sweet chilli sauce, 2 tablespoons dry sherry, 1 tablespoon brown sugar, 6 green shallots, ends trimmed, thinly sliced diagonally, 1/2 cup firmly packed coarsely chopped fresh coriander, Steamed jasmine rice, to serve";
    $scope.details2 = " • Method : ";
    $scope.details3 = "1. Heat the oil in a large wok over high heat until just smoking. Add the crab and stir-fry for 5-7 minutes or until just cooked (the crabmeat will turn white when cooked)";
    $scope.details4 = "Add the garlic, chilli and ginger, and stir-fry for 1 minute or until fragrant. Add the tomato and sweet chilli sauces, sherry and sugar, and stir-fry for 2-3 minutes or until sauce boils and thickens slightly. Remove from heat. Add the green shallot and stir until just wilted. Add the coriander and toss until just combined. Spoon steamed rice among serving bowls. Top with chilli crab and serve immediately.";
  }

  else if (id == 665)
  {
    $scope.stat1 = false;
    $scope.stat2 = true;

    $scope.vid = false;
    $scope.vidUrl = "https://www.youtube.com/embed/N6RudXKUb7U";

    $scope.img1 = "img/t5_1.jpeg";
    $scope.img2 = "";
    $scope.name = "Fish Tacos";
    $scope.details1 = " • Ingredients : ";
    $scope.details2 = " • Tacos : 1 pound white flaky fish, such as mahi mahi or orate, 1/4 cup canola oil, 1 lime, juiced, 1 tablespoons ancho chili powder, 1 jalapeno, coarsely chopped, 1/4 cup chopped fresh cilantro leaves, 8 flour tortillas, ";
    $scope.details3 = " • Garnish : Shredded white cabbage, Hot sauce, Crema or sour cream, Thinly sliced red onion, Thinly sliced green onion, Chopped cilantro leaves, Pureed Tomato Salsa, recipe follows, Pureed Tomato Salsa:,  2 tablespoon peanut oil, 1 small red onion, coarsely chopped, 4 cloves garlic, coarsely chopped, 4 large ripe tomatoes, chopped, 1 serrano chile, 1 jalapeno, sliced, 1 tablespoon chipotle hot sauce, 1 tablespoon Mexican oregano, 1/4 cup chopped fresh cilantro leaves, Salt and pepper";
  $scope.details4 = "1. Garnish : Preheat grill to medium-high heat. Place fish in a medium size dish. Whisk together the oil, lime juice, ancho, jalapeno, and cilantro and pour over the fish. Let marinate for 15 to 20 minutes. Remove the fish from the marinade place onto a hot grill, flesh side down. Grill the fish for 4 minutes on the first side and then flip for 30 seconds and remove. Let rest for 5 minutes then flake the fish with a fork. Place the tortillas on the grill and grill for 20 seconds. Divide the fish among the tortillas and garnish with any or all of the garnishes.";
  $scope.details5 = "2. Pureed Tomato Salsa : Preheat grill or use side burners of the grill. Heat oil in medium saucepan, add onions and garlic and cook until soft. Add tomatoes, serrano and jalapeno and cook until tomatoes are soft, about 15 to 20 minutes. Puree the mixture with a hand-held immersion blender until smooth and cook for an additional 10 to 15 minutes. Add the hot sauce, oregano, cilantro and lime juice and season with salt and pepper, to taste.";

  }

    else if (id == 666)
  {
    $scope.stat1 = false;
    $scope.stat2 = true;

    $scope.img1 = "img/t6_1.jpg";
    $scope.img2 = "";
    $scope.name = "Little Gem cups with prawn & mango salsa";
    $scope.details1 = " • Ingredients : 3 Little Gem lettuces, broken into individual leaves, 1 ripe mango, 1 finely chopped red onion, 1 chopped mild red chilli, juice 1 lime, 4 tbsp light olive oil, 225g pack cooked, peeled tiger prawn, handful chopped coriander";
    $scope.details2 = " • Method : Break the lettuces into individual leaves. Peel and dice the mango and mix with the finely chopped red onion, chopped mild red chilli, the juice 1 lime and the light olive oil. Mix well and set aside at room temperature, covered with cling film. Just before serving, take a the peeled tiger prawns and cut them in half horizontally. Mix into the mango mixture with a handful chopped coriander. To serve, spoon the mixture on to the Little Gem leaves and arrange on a large platter or plate.";
  }

  else if (id == 667)
  {
    $scope.stat1 = false;
    $scope.stat2 = true;

    $scope.img1 = "img/t7_1.png";
    $scope.img2 = "";
    $scope.name = "Rastrelliger (ikan kembung)";
    $scope.details1 = "Rastrelliger is a mackerel genus in the family Scombridae. The three species of Rastrelliger together with the four species of Scomber comprise the tribe Scombrini, known as the true mackerels. It’s the most common fish that local people eat in whole Malaysia eats because its price rate at the market. It also easy to find at any market.";
    $scope.details2 = "The price rate in Sabak Bernam district: RM 9.00 – RM 15.00 per kilogram";
  }

  else if (id == 668)
  {
    $scope.stat1 = false;
    $scope.stat2 = true;

    $scope.img1 = "img/t8_1.png";
    $scope.img2 = "";
    $scope.name = "Torpedo scad (ikan cencaru)";
    $scope.details1 = "The torpedo scad, Megalaspis cordyla, is a species of moderately large marine fish classified in the jack and horse mackerel family, Carangidae. This fish is the second most eaten fish in Malaysia because of its price rate in the market like rastrelliger(ikan kembung), but the size is little bit bigger than rastrelliger(ikan kembung).";
    $scope.details2 = "The price rate in Sabak Bernam district: RM 10.00 – RM 18.00 per kilogram.";
  }

  else if (id == 669)
  {
    $scope.stat1 = false;
    $scope.stat2 = true;

    $scope.img1 = "img/t9_1.jpg";
    $scope.img2 = "";
    $scope.name = "Barramundi (ikan siakap)";
    $scope.details1 = "The barramundi or Asian sea bass is a species of catadromous fish in family Latidae of order Perciformes. The species is widely distributed in the Indo-West Pacific region from Southeast Asia to Papua New Guinea and Northern Australia.";
    $scope.details2 = "The price rate in Sabak Bernam district: RM 16.00 – RM 25.00 per kilogram.";
  }

  else if (id == 670)
  {
    $scope.stat1 = false;
    $scope.stat2 = true;

    $scope.img1 = "img/t10_1.png";
    $scope.img2 = "";
    $scope.name = "Crab (ketam)";
    $scope.details1 = "Crabs are decapod crustaceans of the infraorder Brachyura, which typically have a very short projecting tail, usually entirely hidden under the thorax. The most common type crab that being sell in Sabak Bernam district is the blue colour crab (ketam renjong)";
    $scope.details2 = "The price rate in Sabak Bernam district: RM 16.00 – RM 20.00 per kilogram";
  }

  else
  {
      $scope.stat1 = true;
  $scope.stat2 = false;

    var category = "tips"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);
    });
  }

  $scope.doVideo = function()
  {

    $ionicModal.fromTemplateUrl('templates/modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) { $scope.modal = modal; });
   
  }


})

.controller('RegisterCtrl', function ($scope, $http, $ionicPopup, $location) {

$scope.registerData = {};

$scope.doRegister = function()
{
  var url = $scope.registerData.url;
  var type = $scope.registerData.type;
  var name = $scope.registerData.name;
  var username = $scope.registerData.username;
  var address = $scope.registerData.address;
  var password = $scope.registerData.password;

    $http.get("https://syafiqevo.000webhostapp.com/seaFood/registerUser.php?&url="+url+"&type="+type+"&name="+name+"&username="+username+"&address="+address+"&password="+password).
    success(function(data) {
      
      $scope.data = data;
           $ionicPopup.alert({
           title: 'Success',
           template: 'Please Login'
         })

    .then(function(res) {
    $location.path('/loginTab/login');
   });

      console.log($scope.data);
    });

}

})

.controller('ProfileCtrl', function ($scope, $http, $ionicPopup, $location) {

  var uid = localStorage.getItem("uid");

    $http.get("https://syafiqevo.000webhostapp.com/seaFood/profile.php?&id="+uid).
    success(function(data) {
      
    $scope.url = data[0].url;
    $scope.name = data[0].name;
    $scope.address = data[0].address;

    });

    $http.get("https://syafiqevo.000webhostapp.com/seaFood/galleryProfile.php?&id="+uid).
    success(function(data) {
      
      $scope.data = data;

    })
})

.controller('HomeCtrl', function ($scope, $http, $ionicPopup, $location) {
$scope.options = {
  loop: true,
  effect: 'slide',
  speed: 500,
  autoplay: 3000,
  autoplayDisableOnInteraction: false,
  initialSlide: 0,
}


})

.controller('FeedbackCtrl', function ($scope, $http, $ionicPopup, $location) {

  $scope.doFeedback = function()
  {
    $ionicPopup.alert({
    title: 'Success',
    template: 'Your feedback have been recorded'
    });
  }

}) 

.controller('TransportCtrl', function($scope, $http, $state) {

        var category = "transport"
    $http.get("https://syafiqevo.000webhostapp.com/seaFood/all.php?&category="+category).
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);
    });

    $scope.goDetails = function(id)
    {
      window.localStorage.setItem("transID", id);
      $state.go('app.transDetails');
    }

})

.controller('TransDetailsCtrl', function ($scope, $http, $ionicPopup, $location) {

 var id = localStorage.getItem("transID");

      $scope.initialize = function() {

      if (id == 1)
      {
        var myLatlng = new google.maps.LatLng(3.663225, 101.015344);
        $scope.name = "Homestay Hj. Dorani";
        $scope.address = "No 3 Kampong Hj Dorani , 45300 Sungai Besar , Selangor";
        $scope.phone = "En. Abdul Rahman Bin Daud - 016-6768671";
        $scope.img = "img/ac1.png";
      }
      else if (id == 2)
      {
        var myLatlng = new google.maps.LatLng(3.663225, 101.015344);
        $scope.name = "Homestay Hj. Dorani";
        $scope.address = "No 3 Kampong Hj Dorani , 45300 Sungai Besar , Selangor";
        $scope.phone = "En. Abdul Rahman Bin Daud - 016-6768671";
        $scope.img = "img/ac2.png";
      }

      else if (id == 3)
      {
        var myLatlng = new google.maps.LatLng(3.594306, 101.064100);
        $scope.name = "Homestay Batu 23, Sg. Nibong";
        $scope.address = "Jalan Tengah Masjid, Sungai Nibong ,45400 Sekinchan , Selangor";
        $scope.phone = "Tuan Haji Yusof Bin Haji Sirat - 016-6768671";
        $scope.img = "img/ac3.png";
      }

      else if (id == 4)
      {
        var myLatlng = new google.maps.LatLng(3.522211, 101.090576);
        $scope.name = "Homestay Papitusulem";
        $scope.address = "Balai Raya Parit 7 Baroh, Sungai Leman , 45400 Sekinchan , Sabak Bernam , Selangor";
        $scope.phone = "En. Zainurin Bin Jubri - 017-2712260";
        $scope.img = "img/ac4.png";
      }

      else
      {    

      }


    //sdsdsd
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map3"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }  
 
}); 












