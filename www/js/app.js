// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['self', new RegExp('^(http[s]?):\/\/(w{3}.)?youtube\.com/.+$')]);


  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

        .state('loginTab', {
    url: '/loginTab',
    abstract: true,
  cache: false,
    templateUrl: 'templates/loginTab.html',
  controller: 'LoginCtrl'
  
  })
  
    .state('loginTab.login', {
    url: '/login',
  cache : false,
    views: {
      'login': {
        templateUrl: 'templates/login.html'
      }
    }
  })

  .state('registerTab', {
    url: '/registerTab',
    abstract: true,
    cache: false,
    templateUrl: 'templates/registerTab.html',
    controller : 'RegisterCtrl'
  })
  
  .state('registerTab.register', {
    url: '/register',
  cache : false,
    views: {
      'register': {
        templateUrl: 'templates/register.html'
      }
    }
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.profile', {
      url: '/profile',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile.html',
          controller : 'ProfileCtrl'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

    .state('app.tips', {
    url: '/tips',
    views: {
      'menuContent': {
        templateUrl: 'templates/tips.html',
        controller: 'TipsCtrl'
      }
    }
  })

  .state('app.upload', {
      url: '/upload',
      views: {
        'menuContent': {
          templateUrl: 'templates/upload.html',
          controller : 'UploadCtrl'
        }
      }
    })

  .state('app.tipsDetails', {
      url: '/tipsDetails',
      views: {
        'menuContent': {
          templateUrl: 'templates/tipsDetails.html',
          controller: 'TipsDetailsCtrl'
        }
      }
    })

  .state('app.restaurant', {
      url: '/restaurant',
      views: {
        'menuContent': {
          templateUrl: 'templates/restaurant.html',
          controller: 'RestaurantCtrl'
        }
      }
    })

  .state('app.market', {
      url: '/market',
      views: {
        'menuContent': {
          templateUrl: 'templates/market.html',
          controller: 'MarketCtrl'
        }
      }
    })

  .state('app.attraction', {
      url: '/attraction',
      views: {
        'menuContent': {
          templateUrl: 'templates/attraction.html',
          controller: 'AttractionCtrl'
        }
      }
    })

  .state('app.transport', {
      url: '/transport',
      views: {
        'menuContent': {
          templateUrl: 'templates/transport.html',
          controller: 'TransportCtrl'
        }
      }
    })

    .state('app.restaurantDetails', {
      url: '/restaurantDetails',
      views: {
        'menuContent': {
          templateUrl: 'templates/restaurantDetails.html',
          controller: 'RestaurantDetailsCtrl'
        }
      }
    })

    .state('app.menuList', {
      url: '/menuList',
      views: {
        'menuContent': {
          templateUrl: 'templates/menuList.html',
          controller: 'MenuListCtrl'
        }
      }
    })

    .state('app.gallery', {
      url: '/gallery',
      views: {
        'menuContent': {
          templateUrl: 'templates/gallery.html',
          controller: 'GalleryCtrl'
        }
      }
    })

    .state('app.marketDetails', {
      url: '/marketDetails',
      views: {
        'menuContent': {
          templateUrl: 'templates/marketDetails.html',
          controller: 'MarketDetailsCtrl'
        }
      }
    })

    .state('app.attractionDetails', {
      url: '/attractionDetails',
      views: {
        'menuContent': {
          templateUrl: 'templates/attractionDetails.html',
          controller: 'AttractionDetailsCtrl'
        }
      }
    })

    .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'HomeCtrl'
        }
      }
    })

    .state('app.feedback', {
      url: '/feedback',
      views: {
        'menuContent': {
          templateUrl: 'templates/feedback.html',
          controller: 'FeedbackCtrl'
        }
      }
    })

    .state('app.transDetails', {
      url: '/transDetails',
      views: {
        'menuContent': {
          templateUrl: 'templates/transDetails.html',
          controller: 'TransDetailsCtrl'
        }
      }
    });



  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/loginTab/login');
});
